Splynx add-on for Google Maps
============================

REQUIREMENTS
------------
The minimum requirement that your Web server supports PHP 5.3.0.


INSTALLATION
------------
Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:splynx/splynx-addon-base.git
cd splynx-addon-base
composer global require "fxp/composer-asset-plugin:^1.4.2"
composer install
~~~

Install Splynx Google Maps:
~~~
git clone git@bitbucket.org:splynx/splynx-google-maps.git
cd splynx-google-maps
composer install
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-google-maps/web/ /var/www/splynx/web/google-maps
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-google-maps.addons
~~~

with following content:
~~~
location /google-maps
{
        try_files $uri $uri/ /google-maps/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

MASS GEO CODE
------------
To set coordinates for your customers run this command in add-on directory:
~~~
./yii tools/mass-geocode
~~~

You can use option --force to 'true' to overwrite existing coordinates. 
~~~
./yii tools/mass-geocode --force=true
~~~