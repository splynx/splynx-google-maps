<?php

namespace app\commands;

use splynx\base\BaseInstallController;
use yii\helpers\ArrayHelper;

class InstallController extends BaseInstallController
{
    public function actionIndex()
    {
        parent::actionIndex();

        // edit entry points
        if ($this->isUpdateProcess()) {
            if (!$this->editEntryPoints()) {
                exit('Edit entry points failed!' . "\n");
            }
            $this->deleteOldEntryPoint();
        }
    }

    /**
     * @inheritdoc
     */
    public function getAddOnTitle()
    {
        return 'Google Maps';
    }

    /**
     * @inheritdoc
     */
    public function getModuleName()
    {
        return 'splynx_google_maps_addon';
    }

    /**
     * @inheritdoc
     */
    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\administration\Partners',
                'actions' => ['index'],
            ],
            [
                'controller' => 'api\admin\administration\Locations',
                'actions' => ['index'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
                'actions' => ['index', 'update', 'view'],
            ],
            [
                'controller' => 'api\admin\customers\CustomersOnline',
                'actions' => ['index'],
            ],
            [
                'controller' => 'api\admin\networking\Routers',
                'actions' => ['index', 'update', 'view'],
            ],
            [
                'controller' => 'api\admin\networking\Monitoring',
                'actions' => ['index', 'update', 'view'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerInternetServices',
                'actions' => ['index', 'view'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getEntryPoints()
    {
        return [
            [
                'name' => 'maps',
                'title' => 'Maps',
                'root' => 'controllers\admin\CustomersController',
                'place' => 'admin',
                'type' => 'menu_link',
                'url' => urlencode('/google-maps/maps'),
                'icon' => 'fa-map',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getAdditionalFields()
    {
        return [
            [
                'main_module' => 'customers',
                'module' => $this->getModuleName(),
                'title' => 'GPS',
                'name' => 'coordinates',
                'addon_input_type' => 'text_plus_button',
                'addon_uri' => urlencode('/google-maps/customers'),
                'required' => 0,
                'readonly' => true,
            ],
            [
                'main_module' => 'routers',
                'module' => $this->getModuleName(),
                'title' => 'GPS',
                'name' => 'router_coordinates',
                'addon_input_type' => 'text_plus_button',
                'addon_uri' => urlencode('/google-maps/devices/routers'),
                'required' => 0,
                'readonly' => true,
            ],
            [
                'main_module' => 'monitoring',
                'module' => $this->getModuleName(),
                'title' => 'GPS',
                'name' => 'monitoring_coordinates',
                'addon_input_type' => 'text_plus_button',
                'addon_uri' => urlencode('/google-maps/devices/monitoring'),
                'required' => 0,
                'readonly' => true,
            ],
        ];
    }

    /**
     * Get entry points for edit
     * @return array
     */
    public function getEntryPointsForEdit()
    {
        return [
            [
                'name' => 'maps',
                'type' => 'menu_link',
                'url' => urlencode('/google-maps/maps'),
            ],
        ];
    }

    /**
     * Edit entry points
     *
     * @return bool
     */
    public function editEntryPoints()
    {
        $points = $this->getEntryPointsForEdit();
        if (empty($points)) {
            return true;
        }

        $splynxDir = static::getSplynxDir();

        foreach ($points as $point) {
            $command = "{$splynxDir}system/script/addon edit-entry-point";
            $properties = ArrayHelper::merge($this->getBaseEntryPointProperties(), $point);

            foreach ($properties as $key => $value) {
                $command .= " --$key=\"$value\"";
            }

            $result = exec($command);
            if (!$result) {
                return false;
            }
        }

        return true;
    }

    /**
     * Delete old entry point
     * https://jira.splynx.com/browse/SAGM-14
     */
    public function deleteOldEntryPoint()
    {
        $splynxDir = static::getSplynxDir();
        $command = "{$splynxDir}system/script/addon find-entry-point --module=" . $this->getModuleName() . " --type=\"menu_link\" -- place=\"admin\" --name=\"google_maps\"";
        $result = exec($command . " 2>&1");

        if (!empty($result)) {
            $command = "{$splynxDir}system/script/addon delete-entry-point --id=" . $result;
            $result = exec($command);

            if (!empty($result)) {
                echo "Old entry point successfully deleted\n";
            }
        }
    }
}
