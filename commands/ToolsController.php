<?php

namespace app\commands;

use app\models\Customer;
use Yii;
use yii\base\UserException;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

class ToolsController extends Controller
{
    /** @var null|string */
    public $force = null;

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        return ['force'];
    }

    /**
     * @inheritdoc
     */
    public function optionAliases()
    {
        return ArrayHelper::merge(parent::optionAliases(), [
            'force' => '-force',
        ]);
    }

    /**
     * Check if Google api key is set in config
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (empty(Yii::$app->params['google_api_key'])) {
            throw new UserException(Yii::t('app', 'Google api key is not set. Go to the add-on settings and set it'));
        }

        return parent::beforeAction($action);
    }

    /**
     * Index Mass Geocode
     * Set coordinates by customers address. If you want overwrite existing coordinates set option --force to true
     */
    public function actionMassGeocode()
    {
        echo 'Starting geocode..' . "\n";

        $customer = new Customer();
        /** @var Customer[] $customers */
        $customers = $customer->findAll([]);

        if (empty($customers)) {
            echo 'Error: no customers found!' . "\n";
            return;
        }

        foreach ($customers as $key => $customer) {
            echo "\n" . 'Work with customer "' . $customer->login . '": ' . "\n";

            // Check exists attribute
            if (!array_key_exists('coordinates', $customer->additional_attributes)) {
                echo 'Attribute "coordinates" not found!' . "\n";
                continue;
            }

            // Check `force` and if coordinates already set
            if (!empty($customer->additional_attributes['coordinates']) and $this->force != 'true') {
                echo 'Сoordinates already set!' . "\n";
                continue;
            }

            // Check address
            if (($address = $customer->getAddress()) == null) {
                echo 'No address!' . "\n";
                continue;
            }

            echo 'Found address "' . $address . '": ' . "\n";

            // Try make geocode
            $geoCoder = (new \GoogleMapsGeocoder($address))->setApiKey(Yii::$app->params['google_api_key']);
            $response = $geoCoder->geocode();

            if ($response['status'] != 'OK') {
                echo 'Can\'t make geocode!' . "\n";
                continue;
            } else {
                $data = reset($response['results']);
                $coordinates = $data['geometry']['location']['lat'] . ',' . $data['geometry']['location']['lng'];
            }
            unset($geoCoder);

            // Save coordinates
            if ($customer->updateCoordinates('coordinates', ['coordinates' => $coordinates])) {
                echo 'Coordinates "' . $coordinates . '" saved!' . "\n";
            } else {
                echo 'Failed to save coordinates for customer!' . "\n";
            }
            unset($customers[$key]);
            // Added sleep to prevent be banned from Google
            usleep(rand(500000, 1000000));
            echo "\n";
        }

        echo 'Finishing geocode..' . "\n";
    }
}
