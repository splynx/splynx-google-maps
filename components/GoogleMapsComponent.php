<?php

namespace app\components;

use GoogleMapsGeocoder;
use Yii;
use yii\base\BaseObject;

class GoogleMapsComponent extends BaseObject
{
    /**
     * Google api key
     * @var string $googleApiKey
     */
    public $googleApiKey;

    /**
     * GoogleMaps constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->googleApiKey = Yii::$app->params['google_api_key'];
    }

    /**
     * Find coordinates by geocode, return array response with results
     *
     * @param $address
     * @return array
     */
    public function findCoordinatesByGeocode($address)
    {
        $response = $this->geocode($address);

        if ($response['status'] == 'OK') {
            $data = reset($response['results']);
            return [
                'result' => 'true',
                'message' => 'Coordinates found',
                'coordinates' => $data['geometry']['location']['lat'] . ',' . $data['geometry']['location']['lng']
            ];
        }
        return [
            'result' => 'false',
            'message' => Yii::t('app', 'Can`t find position.')
        ];
    }

    /**
     * Get coordinates by geocode
     *
     * @param $address
     * @return array|\SimpleXMLElement|string
     */
    public function geocode($address)
    {
        $geocoder = (new GoogleMapsGeocoder($address))->setApiKey($this->googleApiKey);
        return $geocoder->geocode();
    }

    /**
     * Validate coordinates
     *
     * @param array|string $coordinates
     * @param int $minLat The problem with mapping is then -85 and not -90 (https://stackoverflow.com/a/13824556)
     * @param int $maxLat The problem with mapping is then 85 and not 90 (https://stackoverflow.com/a/13824556)
     * @param int $minLng
     * @param int $maxLng
     * @return bool
     */
    public static function validateCoordinates($coordinates, $minLat = -85, $maxLat = 85, $minLng = -180, $maxLng = 180)
    {
        if (empty($coordinates)) {
            return false;
        }

        if (!is_array($coordinates)) {
            $reg = '/^(-?)(\d+)((\.)(\d+))?,(-?)(\d+)((\.)(\d+))?$/';
            if (!preg_match_all($reg, $coordinates)) {
                return false;
            }

            $coord = explode(',', $coordinates);
            $coordinates = [
                'lat' => $coord[0],
                'lng' => $coord[1]
            ];
        }

        if (!is_numeric($coordinates['lat'])) {
            return false;
        }
        if ($coordinates['lat'] < $minLat or $coordinates['lat'] > $maxLat) {
            return false;
        }
        if (!is_numeric($coordinates['lng'])) {
            return false;
        }
        if ($coordinates['lng'] < $minLng or $coordinates['lng'] > $maxLng) {
            return false;
        }

        return true;
    }
}
