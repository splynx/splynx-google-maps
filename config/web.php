<?php

return function ($params, $baseDir) {
    return [
        'components' => [
            'request' => [
                'baseUrl' => '/google-maps',
            ],
            'user' => [
                'identityClass' => 'splynx\models\administration\BaseAdministrator',
                'idParam' => 'splynx_admin_id',
                'loginUrl' => '/admin/login/?return=%2Fgoogle-maps',
                'enableAutoLogin' => false,
            ],
        ],
    ];
};
