<?php

namespace app\controllers;

use app\components\GoogleMapsComponent;
use app\models\Customer;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class CustomersController
 *
 * index: displays map for all customers
 * customer: set coordinates from  address (geocode) and set it to  the customer
 *
 * @package app\controllers
 */
class CustomersController extends Controller
{
    public $default_center = [
        'lat' => 30,
        'lng' => 15
    ];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * Check if Google api key is set in config
     * @inheritdoc
     * @throws UserException
     */
    public function beforeAction($action)
    {
        if (empty(Yii::$app->params['google_api_key'])) {
            throw new UserException(Yii::t('app', 'Google api key is not set. Go to the <a href="/admin/config/modules/list--view?module=splynx_google_maps_addon" target="_parent">add-on settings</a> and set it!'));
        }

        return parent::beforeAction($action);
    }

    /**
     * Find coordinates by  address (geocode), set it to  the customer
     *
     * @param integer $id
     * @param string $geocode
     * @return string | array
     */
    public function actionIndex($id, $geocode = 'false')
    {
        $this->getView()->title = Yii::t('app', 'Customers coordinates');
        $customer = null;
        if (!empty($id)) {
            $customer = Customer::getById($id);
        }

        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = Yii::$app->request->post();

            // Try to get coordinates by geocode
            if ($geocode == 'true') {
                if (!empty($post['address'])) {
                    return (new GoogleMapsComponent())->findCoordinatesByGeocode($post['address']);
                } else {
                    return [
                        'result' => 'false',
                        'message' => Yii::t('app', 'Set address to input and try again.'),
                    ];
                }
            }

            // Work with customers coordinates
            if (!empty($post)) {
                // Save coordinates
                if (empty($id)) {
                    // if id empty it new record and set coordinates to customer form
                    return [
                        'result' => 'true',
                        'message' => Yii::t('app', 'Coordinates successfully set'),
                    ];
                } else {
                    if ($customer->updateCoordinates('coordinates', $post)) {
                        return [
                            'result' => 'true',
                            'message' => Yii::t('app', 'Coordinates successfully updated'),
                        ];
                    } else {
                        return [
                            'result' => 'false',
                            'message' => $customer->getErrors(),
                        ];
                    }
                }
            }
        }

        // Default
        $markers = [];
        if ($customer != null) {
            // Get coordinates
            foreach ($customer->getCoordinates() as $coordinate) {
                if (GoogleMapsComponent::validateCoordinates($coordinate)) {
                    $item = $coordinate;
                    $item['login'] = $customer->login;
                    $item['name'] = $customer->name;
                    $item['color'] = $customer->getMarkerColor();
                    $item['status'] = ucwords($customer->getStatus());
                    $markers[] = $item;
                }
            }
        }


        return $this->render('customer', [
            'customer' => $customer,
            'markers' => $markers,
            'default_center' => $this->default_center,
        ]);
    }
}
