<?php

namespace app\controllers;

use app\components\GoogleMapsComponent;
use app\models\devices\MonitoringDevice;
use app\models\devices\Routers;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class DevicesController
 *
 * routers: displays map for router, set coordinates and polygon for it
 * monitoring: displays map for monitoring devices and set coordinates and polygon for it
 *
 * @package app\controllers
 * @author Vasyl Kotovets
 * @version 2.0
 */
class DevicesController extends Controller
{
    public $default_center = [
        'lat' => 30,
        'lng' => 15,
    ];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * Check if Google api key is set in config
     * @inheritdoc
     * @throws UserException
     */
    public function beforeAction($action)
    {
        if (empty(Yii::$app->params['google_api_key'])) {
            throw new UserException(Yii::t('app', 'Google api key is not set. Go to the <a href="/admin/config/modules/list--view?module=splynx_google_maps_addon" target="_parent">add-on settings</a> and set it!'));
        }

        return parent::beforeAction($action);
    }

    /**
     * Action routers
     * Displays map for router, set coordinates for it
     *
     * @param integer $id Id of router
     * @param string $geocode
     * @return string | array
     */
    public function actionRouters($id, $geocode = 'false')
    {
        $this->getView()->title = Yii::t('app', 'Routers coordinates');

        $router = null;
        if (!empty($id)) {
            $router = Routers::getById($id);
        }

        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = Yii::$app->request->post();

            // Try to get coordinates by geocode
            if ($geocode == 'true') {
                if (!empty($post['address'])) {
                    return (new GoogleMapsComponent())->findCoordinatesByGeocode($post['address']);
                } else {
                    return [
                        'result' => 'false',
                        'message' => Yii::t('app', 'Set address to input and try again.'),
                    ];
                }
            }

            // Work with routers coordinates
            if (!empty($post)) {
                // if id empty it new record and set coordinates to customer form
                if (empty($id)) {
                    return [
                        'result' => 'true',
                        'message' => Yii::t('app', 'Coordinates successfully set'),
                    ];
                }

                if ($router->updateCoordinates('router_coordinates', $post)) {
                    return [
                        'result' => 'true',
                        'message' => Yii::t('app', 'Coordinates successfully updated'),
                    ];
                } else {
                    return [
                        'result' => 'false',
                        'message' => $router->getErrors(),
                    ];
                }
            }
        }
        $point = [];

        if ($router !== null and GoogleMapsComponent::validateCoordinates($router->getCoordinates())) {
            $point = ArrayHelper::merge($router->getCoordinates(), [
                'title' => $router->title,
                'ip' => $router->ip,
            ]);
        }

        return $this->render('devices-map', [
            'point' => $point,
            'device' => $router,
            'default_center' => $this->default_center,
            'attribute' => 'router_coordinates',
        ]);
    }

    /**
     * Action Monitoring
     * Displays monitoring map, set coordinates for it
     *
     * @param integer $id Id of monitoring
     * @param string $geocode
     * @return string | array
     */
    public function actionMonitoring($id, $geocode = 'false')
    {
        $this->getView()->title = Yii::t('app', 'Monitoring Devices coordinates');

        $device = null;
        if (!empty($id)) {
            $device = MonitoringDevice::getById($id);
        }

        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = Yii::$app->request->post();

            // Try to get coordinates by geocode
            if ($geocode == 'true') {
                if (!empty($post['address'])) {
                    return (new GoogleMapsComponent())->findCoordinatesByGeocode($post['address']);
                } else {
                    return [
                        'result' => 'false',
                        'message' => Yii::t('app', 'Set address to input and try again.'),
                    ];
                }
            }

            // Work with routers coordinates
            if (!empty($post)) {
                // if id empty it new record and set coordinates to customer form
                if (empty($id)) {
                    return [
                        'result' => 'true',
                        'message' => Yii::t('app', 'Coordinates successfully set'),
                    ];
                }

                if ($device->updateCoordinates('monitoring_coordinates', $post)) {
                    return [
                        'result' => 'true',
                        'message' => Yii::t('app', 'Coordinates successfully updated'),
                    ];
                } else {
                    return [
                        'result' => 'false',
                        'message' => $device->getErrors(),
                    ];
                }
            }
        }
        $point = [];

        if ($device !== null and GoogleMapsComponent::validateCoordinates($device->getCoordinates())) {
            $point = ArrayHelper::merge($device->getCoordinates(), [
                'title' => $device->title,
                'ip' => $device->ip,
            ]);
        }

        return $this->render('devices-map', [
            'point' => $point,
            'device' => $device,
            'default_center' => $this->default_center,
            'attribute' => 'monitoring_coordinates',
        ]);
    }
}
