<?php

namespace app\controllers;

use app\models\Customer;
use app\models\devices\MonitoringDevice;
use app\models\devices\Routers;
use app\models\Location;
use app\models\Partner;
use splynx\models\Admin;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class CustomersController
 *
 * index: displays map for all customers and routers
 *
 * @package app\controllers
 */
class MapsController extends Controller
{
    public $default_center = [
        'lat' => 30,
        'lng' => 15
    ];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * Check if Google api key is set in config
     * @inheritdoc
     * @throws UserException
     */
    public function beforeAction($action)
    {
        if (empty(Yii::$app->params['google_api_key'])) {
            throw new UserException(Yii::t('app', 'Google api key is not set. Go to the <a href="/admin/config/modules/list--view?module=splynx_google_maps_addon" target="_parent">add-on settings</a> and set it!'));
        }

        return parent::beforeAction($action);
    }

    /**
     * Action index
     * Displays map with all customers coordinates
     *
     * @return string | array
     */
    public function actionIndex()
    {
        $this->getView()->title = Yii::t('app', 'Maps');
        $partner = $location = null;
        $customers = $routers = $monitoring = [];
        $customersMarkers = $routersMarkers = $monitoringMarkers = [];

        $partners = (new Partner())->getAllAsArray();
        $locations = (new Location())->getAllAsArray();

        /**@var Admin $admin */
        $admin = Yii::$app->getUser()->getIdentity();
        $partnerId = $admin->partner_id;

        $search = [];

        if (!empty($partnerId)) {
            $search['partner_id'] = $partnerId;
            $partner = $partnerId;
        }

        if (($post = Yii::$app->getRequest()->post()) !== []) {
            // Set partner id to search
            if (empty($partnerId) and $post['partner'] and isset($partners[$post['partner']])) {
                $search['partner_id'] = $post['partner'];
                $partner = $post['partner'];
            }

            // Set location id to search
            if ($post['location'] and isset($locations[$post['location']])) {
                $search['location_id'] = $post['location'];
                $location = $post['location'];
            }

            if (!empty($post['show_markers'])) {
                switch ($post['show_markers']) {
                    case 'customer':
                        $customers = Customer::getAllWithCoordinates($search);
                        break;
                    case 'routers':
                        // remove partner id
                        unset($search['partner_id']);
                        $routers = Routers::getAllWithCoordinates($search);
                        break;
                    case 'routers_customer':
                        $customers = Customer::getAllWithCoordinates($search);
                        // remove partner id
                        unset($search['partner_id']);
                        $routers = Routers::getAllWithCoordinates($search);
                        break;
                    case 'monitoring':
                        // remove partner id
                        unset($search['partner_id']);
                        $monitoring = MonitoringDevice::getAllWithCoordinates($search);
                        break;
                }
            } else {
                $customers = Customer::getAllWithCoordinates($search);

                // remove partner id
                unset($search['partner_id']);
                $routers = Routers::getAllWithCoordinates($search);
                $monitoring = MonitoringDevice::getAllWithCoordinates($search);
            }
        } else {
            // Load all customers
            if (empty($customers)) {
                $customers = Customer::getAllWithCoordinates($search);
                // remove partner id
                unset($search['partner_id']);
                $routers = Routers::getAllWithCoordinates($search);
                $monitoring = MonitoringDevice::getAllWithCoordinates($search);
            }
        }

        // Create array with customersMarkers
        if (!empty($customers)) {
            $customersMarkers = Customer::getMarkers($customers);
        }

        // filter routers by partner
        if (!empty($partner)) {
            $routers = Routers::filterByPartner($routers, $partner);
            $monitoring = MonitoringDevice::filterByPartner($monitoring, $partner);
        }

        // get routers for customersMarkers
        if (!empty($routers)) {
            $routersMarkers = Routers::getMarkers($routers, (isset($post['show_markers']) and $post['show_markers'] == 'routers_customer'));
        }
        if (!empty($monitoring)) {
            $monitoringMarkers = MonitoringDevice::getMarkers($monitoring);
        }

        // if request is post we return  list of customersMarkers
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'customersMarkers' => $customersMarkers,
                'routersMarkers' => $routersMarkers,
                'monitoringMarkers' => $monitoringMarkers,
            ];
        }

        return $this->render('all-map', [
            'showPartners' => $partnerId == 0 ? true : false,
            'partners' => $partners,
            'partner' => $partner,
            'locations' => $locations,
            'location' => $location,
            'customersMarkers' => $customersMarkers,
            'routersMarkers' => $routersMarkers,
            'monitoringMarkers' => $monitoringMarkers,
            'default_center' => $this->default_center
        ]);
    }
}
