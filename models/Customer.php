<?php

namespace app\models;

use app\components\GoogleMapsComponent;
use splynx\helpers\ApiHelper;
use splynx\models\customer\BaseCustomer;

class Customer extends BaseCustomer
{
    private $_coordinates = [];

    /**
     * Get all customers with coordinates
     *
     * @param array $mainAttributes
     * @return Customer[]|array
     */
    public static function getAllWithCoordinates($mainAttributes = [])
    {
        /** @var Customer[] $customers */
        $customers = (new static())->findAll($mainAttributes, [
            'coordinates' => ['!=', '']
        ]);

        foreach ($customers as &$customer) {
            $customer->initCoordinates();
        }

        return $customers;
    }

    /**
     * Set coordinates from additional attributes if it exist
     */
    public function initCoordinates()
    {
        $coordinates = [];
        if (isset($this->additional_attributes['coordinates']) and !empty($this->additional_attributes['coordinates'])) {
            if (strpos($this->additional_attributes['coordinates'], ';') !== false) {
                $coordinates = explode(';', $this->additional_attributes['coordinates']);
            } else {
                $coordinates[] = $this->additional_attributes['coordinates'];
            }

            foreach ($coordinates as $coordinate) {
                if (strpos($coordinate, ',') !== false) {
                    list($lat, $lng) = explode(',', $coordinate);
                    $this->_coordinates[] = [
                        'lat' => $lat,
                        'lng' => $lng
                    ];
                }
            }
        }
    }

    /**
     * Find customer by id
     * @param $id
     * @return Customer|null
     */
    public static function getById($id)
    {
        $customer = (new static())->findById($id);

        if ($customer) {
            $customer->initCoordinates();
            return $customer;
        } else {
            return null;
        }
    }

    /**
     * Update customers coordinates
     *
     * @param string $attribute
     * @param array $data
     * @return bool
     */
    public function updateCoordinates($attribute, $data)
    {
        if (!isset($data['coordinates'])) {
            return false;
        }

        $coordinates = $this->prepareCoordinates($data['coordinates']);

        if (!$this->validateCoordinates($coordinates)) {
            return false;
        }

        $this->additional_attributes[$attribute] = $coordinates;

        return $this->save();
    }

    /**
     * Validate coordinates
     *
     * @param string $coordinates
     * @return bool
     */
    public function validateCoordinates($coordinates)
    {
        if (empty($coordinates) and $coordinates != '0') {
            return true;
        }

        $coordinatesArray = explode(';', $coordinates);

        foreach ($coordinatesArray as $value) {
            if (!GoogleMapsComponent::validateCoordinates($value)) {
                $this->addError('coordinates', 'Invalid coordinates');
                return false;
            }
        }

        if ($this->checkDuplicateCoordinates($coordinatesArray)) {
            $this->addError('coordinates', 'Duplicate  coordinates');
            return false;
        }

        return true;
    }

    /**
     * Prepare coordinates
     *
     * @param string $coordinates
     * @return string
     */
    public function prepareCoordinates($coordinates)
    {
        $coordinatesArray = explode(';', $coordinates);
        return implode(';', array_diff($coordinatesArray, ['']));
    }

    /**
     * Check identical coordinates
     *
     * @param array $coordinates
     * @return bool|array
     */
    public function checkDuplicateCoordinates($coordinates)
    {
        return count(array_unique($coordinates)) < count($coordinates);
    }

    /**
     * Get Customers address
     * @return null|string
     */
    public function getAddress()
    {
        $params = ['zip_code', 'city', 'street_1'];
        $address_parts = [];

        foreach ($params as $param) {
            if (!empty($this->$param)) {
                $address_parts[] = $this->$param;
            }
        }

        if (!empty($address_parts)) {
            return implode(', ', $address_parts);
        }

        return null;
    }

    /**
     * Get customer status
     * @return string
     */
    public function getStatus()
    {
        if ($this->isOnline()) {
            return 'online';
        }

        return $this->status;
    }

    /**
     * Get marker color
     * @return string
     */
    public function getMarkerColor()
    {
        switch ($this->getStatus()) {
            case 'new':
                $color = 'blue';
                break;

            case 'blocked':
                $color = 'red';
                break;

            case 'disabled':
                $color = 'pink';
                break;

            case 'online':
                $color = 'green';
                break;

            case 'active':
            default:
                $color = 'ltblue';
                break;
        }

        return $color;
    }

    /** @var array|null List of online customers */
    private static $_online;

    /**
     * Check if customer is online
     * @return bool
     */
    public function isOnline()
    {
        if (self::$_online === null) {
            $result = ApiHelper::get('admin/customers/customers-online');

            if ($result['result'] and !empty($result['response'])) {
                foreach ($result['response'] as $item) {
                    self::$_online[] = $item['customer_id'];
                }
            } else {
                self::$_online = [];
            }
        }

        return in_array($this->id, self::$_online);
    }

    /**
     * Get coordinates
     * @return array
     */
    public function getCoordinates()
    {
        return $this->_coordinates;
    }

    /**
     * Set coordinates
     * @param string $coordinates
     */
    public function setCoordinates($coordinates)
    {
        $this->_coordinates = $coordinates;
    }

    /**
     * Get coordinates for input in string format
     * @return  string
     */
    public function getCoordinatesForInput()
    {
        $value = '';
        if (!empty($this->_coordinates)) {
            if (count($this->_coordinates) > 1) {
                $array = [];
                foreach ($this->_coordinates as $coordinate) {
                    $array[] = $coordinate['lat'] . ',' . $coordinate['lng'];
                }
                $value = implode(';', $array);
            } else {
                $coordinate = reset($this->_coordinates);
                $value = $coordinate['lat'] . ',' . $coordinate['lng'];
            }
        }

        return $value;
    }

    /**
     * Get Markers
     *
     * @param static[] $customers
     * @return array
     */
    public static function getMarkers($customers)
    {
        $markers = [];

        foreach ($customers as $customer) {
            foreach ($customer->getCoordinates() as $coordinate) {
                if (GoogleMapsComponent::validateCoordinates($coordinate)) {
                    $item = $coordinate;
                    $item['login'] = $customer->login;
                    $item['name'] = $customer->name;
                    $item['color'] = $customer->getMarkerColor();
                    $item['status'] = ucwords($customer->getStatus());
                    $item['id'] = $customer->id;
                    $markers[] = $item;
                }
            }
        }

        return $markers;
    }
}
