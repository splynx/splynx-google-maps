<?php

namespace app\models;

use splynx\models\administration\BasePartner;
use Yii;
use yii\helpers\ArrayHelper;

class Partner extends BasePartner
{
    /**
     * Get all partners array if partners not found return empty array
     * //     * [
     *      '0' => 'All'
     *      'id' => 'name,
     * ]
     * @return array
     */
    public function getAllAsArray()
    {
        $items = static::findAll([]);

        if (!empty($items)) {
            return ArrayHelper::merge(
                [0 => Yii::t('app', 'All')],
                ArrayHelper::map($items, 'id', 'name')
            );
        }

        return [];
    }
}
