<?php

namespace app\models\devices;

use splynx\base\BaseActiveApi;
use app\components\GoogleMapsComponent;

class BaseDevice extends BaseActiveApi
{
    /**
     * Central coordinate for Router
     * @var array $_coordinates
     */
    protected $_coordinates;

    public $additional_attributes = [];

    /**
     * Name of coordinates attributes from AF
     * @var string
     */
    protected static $attributeName = '';

    /**
     * Set coordinates from additional attributes if it exist
     *
     * Additional attribute name
     * @param string $attribute
     */
    public function initCordinates($attribute)
    {
        $coordinates = $this->additional_attributes[$attribute];
        if (isset($coordinates) and !empty($coordinates)) {
            // Set coordinates
            if (strpos($coordinates, ',') !== false) {
                list($lat, $lng) = explode(',', $coordinates);
                $this->_coordinates = [
                    'lat' => $lat,
                    'lng' => $lng,
                ];
            }
        }
    }

    /**
     * Update device coordinates
     *
     * @param string $attribute
     * @param array $data
     * @return bool
     */
    public function updateCoordinates($attribute, $data)
    {
        if (!isset($data[$attribute])) {
            return false;
        }

        if (!$this->validateCoordinates($data[$attribute])) {
            $this->addError(static::$attributeName, 'Invalid Coordinates');
            return false;
        }

        $this->additional_attributes[$attribute] = $data[$attribute];

        return $this->save();
    }

    /**
     * Validate coordinates
     *
     * @param string $coordinates
     * @return bool
     */
    public function validateCoordinates($coordinates)
    {
        if (empty($coordinates) and $coordinates != '0') {
            return true;
        }

        if (!GoogleMapsComponent::validateCoordinates($coordinates)) {
            return false;
        }

        return true;
    }

    /**
     * Get point coordinates
     * @return array
     */
    public function getCoordinates()
    {
        return $this->_coordinates;
    }

    /**
     * Get coordinate attributes name
     * @return string
     */
    public function getCoordinatesAttributeName()
    {
        return static::$attributeName;
    }

    /**
     * Get all devices with coordinates
     * Used for Maps
     *
     * @param array $mainAttributes
     * @return static[]|null
     */
    public static function getAllWithCoordinates($mainAttributes = [])
    {
        /** @var static[] $routers */
        $routers = (new static())->findAll($mainAttributes, [
            static::$attributeName => ['!=', '']
        ]);

        foreach ($routers as &$router) {
            $router->initCordinates(static::$attributeName);
        }

        return $routers;
    }

    /**
     * Find device by id
     * @param $id
     * @return static|null
     */
    public static function getById($id)
    {
        $device = (new static())->findById($id);

        if ($device) {
            $device->initCordinates(static::$attributeName);
            return $device;
        } else {
            return null;
        }
    }

    /**
     * Return coordinates for input
     * @return string
     */
    public function getCoordinatesForInput()
    {
        return $this->additional_attributes[static::$attributeName];
    }

    /**
     * Filter device by partner
     *
     * @param  static[] $devices
     * @param integer $partnerId
     * @return static[]|array
     */
    public static function filterByPartner($devices, $partnerId)
    {
        if (empty($devices)) {
            return [];
        }

        foreach ($devices as $key => $device) {
            if (!empty($device->partners_ids)) {
                if (!in_array($partnerId, $device->partners_ids)) {
                    unset($devices[$key]);
                }
            }
        }

        return $devices;
    }
}
