<?php

namespace app\models\devices;

use app\components\GoogleMapsComponent;

class MonitoringDevice extends BaseDevice
{
    public $id;
    public $title;
    public $parent_id;
    public $producer;
    public $model;
    public $ip;
    public $address;

    public $partners_ids = [];

    protected static $apiUrl = 'admin/networking/monitoring';

    protected static $attributeName = 'monitoring_coordinates';

    /**
     * Get markers
     *
     * @param static[] $devices
     * @return array
     */
    public static function getMarkers($devices)
    {
        $markers = [];

        foreach ($devices as $device) {
            if (GoogleMapsComponent::validateCoordinates($device->getCoordinates())) {
                $item = $device->getCoordinates();
                $item['id'] = $device->id;
                $item['title'] = $device->title;
                $item['ip'] = $device->ip;
                $markers[] = $item;
            }
        }

        return $markers;
    }
}