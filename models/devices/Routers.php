<?php

namespace app\models\devices;

use app\components\GoogleMapsComponent;
use app\models\Customer;
use splynx\helpers\ApiHelper;

class Routers extends BaseDevice
{
    public $id;
    public $title;
    public $model;
    public $location_id;
    public $address;
    public $ip;
    public $authorization_method_id;
    public $nas_type;
    public $nas_ip;
    public $radius_secret;

    public $partners_ids = [];

    protected static $apiUrl = 'admin/networking/routers';
    protected static $servicesApiCall = 'admin/customers/customer/0/internet-services';
    protected static $attributeName = 'router_coordinates';

    /**
     * Get markers
     *
     * @param static[] $routers
     *
     * If $zone = true we are finding customers for routers
     * @param bool $zone
     * @return array
     */
    public static function getMarkers($routers, $zone = false)
    {
        $markers = [];

        foreach ($routers as $router) {
            if (GoogleMapsComponent::validateCoordinates($router->getCoordinates())) {
                $item = $router->getCoordinates();
                $item['id'] = $router->id;
                $item['title'] = $router->title;
                $item['ip'] = $router->ip;
                if ($zone) {
                    $item['customer_ids'] = $router->getCustomersIds();
                }
                $markers[] = $item;
            }
        }

        return $markers;
    }

    /**
     * Get customers ids who use this router
     *
     * @return array
     */
    public function getCustomersIds()
    {
        $customerIds = [];

        $services = $this->getServices();
        if (empty($services)) {
            return $customerIds;
        }
        foreach ($services as $service) {
            $customer = (new Customer())->findOne(
                ['id' => $service['customer_id']],
                ['coordinates' => ['!=', '']]
            );

            if (!empty($customer)) {
                $customer->initCoordinates();
                if (!isset($customerIds[$customer->id])) {
                    $customerIds[$customer->id] = $customer->getCoordinates();
                }
            }
        }

        return $customerIds;
    }

    /**
     * Get router`s services
     *
     * @return array
     */
    public function getServices()
    {
        $result = ApiHelper::search(static::$servicesApiCall, [
            'main_attributes' => [
                'router_id' => $this->id,
            ]
        ]);
        if ($result['result'] == true and !empty($result['response'])) {
            return $result['response'];
        }

        return null;
    }
}